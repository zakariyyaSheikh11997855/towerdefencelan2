﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TowerTracking : MonoBehaviour
{

    public float speed = 5;
    GameObject target;
    GameObject first = null;
    public GameObject EndZone;
    Vector3 lastKnownPosition = Vector3.zero;
    Quaternion lookAtRotation;
    public int trackRadius;
    SphereCollider sc;
    List<GameObject> trackingList = new List<GameObject>();
    float distance;

    // Use this for initialization
    void Start()
    {
        sc = GetComponent<SphereCollider>();
        sc.radius = trackRadius;
        distance = Mathf.Infinity;
    }

    // Update is called once per frame
    void Update()
    {

        //target = GameObject.FindWithTag("Enemy");

        if (trackingList.Count > 0)
        {
            target = FindFirst();
            //target = GameObject.FindWithTag("Enemy");
        }
        else
        {
            target = null;
        }



        if (target != null)
        {

            //target = FindFirst();
            //if ((Vector3.Distance(transform.position, target.transform.position) < maxRange)
            //&& (Vector3.Distance(transform.position, target.transform.position) > minRange))

            if (lastKnownPosition != target.transform.position)
            {
                lastKnownPosition = target.transform.position;
                lookAtRotation = Quaternion.LookRotation(lastKnownPosition - transform.position);
            }

            if (transform.rotation != lookAtRotation)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, lookAtRotation, speed * Time.deltaTime);
            }

        }
        else
        {
            target = null;
        }
        print(trackingList.Count);

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            trackingList.Add(other.gameObject);


        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Enemy")
        {
            trackingList.Remove(other.gameObject);
        }
    }

    public GameObject FindFirst()
    {
        distance = Mathf.Infinity;
        foreach (GameObject enemy in trackingList)
        {
            Vector3 diff = enemy.transform.position - EndZone.transform.position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                first = enemy;
                distance = curDistance;
                //target = go;

            }
        }
        return first;
    }


}
